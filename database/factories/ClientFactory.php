<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'nombre' => $faker->company,
        'direccion' => $faker->address,
        'poblacion' => $faker->city,
        'contacto' => $faker->name,
        'sector_id' => random_int(1,15),
        'delegation_id' => random_int(1,2),
        'descripcion' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'email' => $faker->unique()->safeEmail,
        
    ];
});
