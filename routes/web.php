<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('banner-top/createWithSector/{sector_id}', ['uses' => 'Voyager\BannerTopController@createWithSector', 'as' => 'createWithSector']);
    Route::get('banner-top/createWithDelegation/{delegation_id}', ['uses' => 'Voyager\BannerTopController@createWithDelegation', 'as' => 'createWithDelegation']);
    Route::get('banner-detail/createWithSector/{sector_id}', ['uses' => 'Voyager\BannerDetailController@createWithSector', 'as' => 'createWithSector']);
    Route::get('banner-detail/createWithDelegation/{delegation_id}', ['uses' => 'Voyager\BannerDetailController@createWithDelegation', 'as' => 'createWithDelegation']);
});

Auth::routes();


Route::middleware(['auth'])->group(function(){
    Route::get('/empresas', ['uses' => 'HomeController@asociados', 'empresas']);
    Route::get('/empresas/{id?}', ['uses' => 'HomeController@asociados', 'empresas']);
    Route::get('/empresassector/{sector?}', ['uses' => 'HomeController@asociadosPorSector', 'empresasSector']);
    Route::get('/empresasdelegation/{delegation?}', ['uses' => 'HomeController@asociadosPorDelegacion', 'empresasDelegacion']);
    Route::view('/tarifas', 'precios')->name('precios');

    Route::get('/chat', function(){
        return view('chat.index');
    });
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', function(){
    auth()->logout();
    return redirect('home');
});

Route::view('/clubdaffaires', 'clubdaffaires')->name('clubdaffaires');
Route::view('/quienes-somos', 'quienessomos')->name('quienessomos');
Route::view('/contacto', 'contacto')->name('contacto');
Route::view('/organizacion', 'organizacion')->name('organizacion');
Route::view('/mensajepresidente', 'mensajepresidente')->name('mensajepresidente');

Route::post('/contactar', 'HomeController@contactar')->name('contactar');
Route::get('/imagenSector/{nombre}', function($nombre){
    $sector = App\Sector::where('nombre',$nombre)->with('bannertop')->get();
    return $sector;
});

Route::get('/creartodos', function(){
    $clientes = \App\Client::all();
    foreach ($clientes as $key => $cliente) {
        if ($cliente->servicios) {
            if ( ! App\User::where('email', '=', $cliente->servicios )->exists() ) {
                // user found
                $user = new App\User();
                $user->password = Hash::make('s1r0p3');
                $user->name = strtolower($cliente->nombre);
                $user->email = $cliente->servicios;
                $user->save();
             }
        }
    }
});

Route::get('/prumail', function(){
    Mail::raw('Text', function ($message){
        $message->to('planetamongoloid@gmail.com');
    });
});


Route::get('/contacts', 'ContactsController@get');
Route::get('/conversation/{id}', 'ContactsController@getMessagesFor');
Route::get('/getTotalMessagesFor', 'ContactsController@getTotalMessagesFor');
Route::post('/conversation/send', 'ContactsController@send');
