@extends('app')

@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
											<div class="v3-top-ri">
													<ul>
														<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																@auth
																		<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
																@endauth
																@guest
																		<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
																@endguest


													</ul>
											</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					<li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>
<section class="inn-page-bg">
		<div class="container">
			<div class="row">
				<div class="inn-pag-ban">
					<h2>Organización</h2>
					<!-- <h5>Grow your business by getting relevant and verified leads</h5> </div> -->
			</div>
		</div>
	</section>
	<section class="p-about com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-about pad-bot-red-40">
						<h3>Consejo de administración</h3>
            <span>Está compuesto por directivos de grupos franceses presentes en España o a personas directamente implicadas en los intercambios entre Francia y España. Participa estrechamente en la definición de las orientaciones estratégicas de La Chambre.</span>
            <div class="com-padd">
              <h4>Presidente de Honor</h4>
              <p>Excmo. Embajador de Francia en España: Sr. D. Yves SAINT-GEOURS</p>
            </div>
            <div>
              <h4>Presidentes Honorarios</h4>
              <p>Sr. D. Pierre PIERART</br>
              Sr. D. Etienne OBERT de THIEUSIES</br>
              Sr. D. Domingo SAN FELIPE</br>
              Sr. D. Francis HUSS</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="page-about"> <img src="images/about.jpg" alt=""> </div>
          </div>
        </div>
      </div>



		  <div class="row">
        <div class="col-md-12">
          <div class="col-md-12">
            <div class="con-com">
              <div class="ficha-organizacion">
                <img src="images/Laurent_Paillassot.jpg" alt="">
                <div class="ficha">
                <h4 class="con-tit-top-o">Presidente</h4>
                <h5>D. Laurent PAILLASSOT</h5> <p>CEO - ORANGE ESPAÑA</p><span>
              </div>
    				</div>
          </div>
        </div>




        <div class="col-md-12">
          <div class="con-com">
            <div class="ficha-organizacion">
              <img src="images/Alejandro_Alonso.jpg" alt="">
              <div class="ficha">
              <h4 class="con-tit-top-o">Vicepresidente y Secretario General</h4>
              <h5>D. Alejandro ALONSO DREGI</h5> <p>Socio - DENTONS</p><span>
          </div>
        </div>
			</div>
		</div>



      <div class="col-md-12">
        <div class="con-com">
          <div class="ficha-organizacion">
            <img src="images/Creisquer.jpg" alt="">
            <div class="ficha">
            <h4 class="con-tit-top-o">Vicepresidente y Tesorero</h4>
            <h5>D. Stéphane DE CREISQUER</h5> <p></p><span>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="com-padd"><h4>Vice-presidentes</h4></div>
      <div class="col-md-6">
      <div class="con-com">
        <div class="ficha-organizacion">
          <img src="images/sara_Bieger.png" alt="">
          <div class="ficha">
          <!-- <h4 class="con-tit-top-o">Vicepresidente y Tesorero</h4> -->
          <h5>Dª Sara BIEGER</h5> <p>Socia Directora - ALTO PARTNERS</br>Presidenta del Club d'Affaires France-Espagne</p><span>
        </div>
      </div>
    </div>
  </div>

<div class="col-md-6">
    <div class="con-com">
      <div class="ficha-organizacion">
        <img src="images/DILLMANN.jpg" alt="">
        <div class="ficha">
        <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
        <h5>D. Philippe DILLMANN</h5> <p>Presidente - DBP</p><span>
      </div>
    </div>
  </div>
</div>


<div class="col-md-6">
  <div class="con-com">
    <div class="ficha-organizacion">
      <img src="images/Ricardo_de_Ramon.jpg" alt="">
      <div class="ficha">
      <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
      <h5>D. Ricardo DE RAMÓN</h5><span>
    </div>
  </div>
</div>
</div>

<div class="col-md-6">
	<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/Rignault.jpg" alt="">
    <div class="ficha">
    <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
    <h5>D. Jean-Paul RIGNAULT</h5> <p>Consejero Delegado - AXA</p>
  </div>
</div>
</div>

</div>


</div>




<div class="row">
	<div class="com-padd"><h4>Administradores</h4></div>
<div class="col-md-6">
  <div class="con-com">
    <div class="ficha-organizacion">
      <img src="images/Angel_Benguigui.jpg" alt="">
      <div class="ficha">
      <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
      <h5>Sr. D. Ángel BENGUIGUI</p> <p>Presidente - ECONOCOM</p>
    </div>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/Cecilia-Boned-Lloveras_909ee7a3d0.jpg" alt="">
    <div class="ficha">
    <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
    <h5>Sra. Dña. Cecilia BONED</p> <p>Country Head GRUPO BNP PARIBAS EN ESPAÑA</p>
  </div>
</div>
</div>
</div>


<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
  <img src="images/Olivier_Bernon_54cd3bcbcc.jpg" alt="">
  <div class="ficha">
  <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
  <h5>Sr. D. Olivier BERNON</p> <p>Director General de UP SPAIN</p>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
<img src="images/CANADAS__ROCA_c4f51463c4.jpg" alt="">
<div class="ficha">
<!-- <h4 class="con-tit-top-o">Presidente</h4> -->
<h5>Sr. D. Antonio CAÑADAS</h5> <p>Socio Director - CAÑADAS ABOGADOS</p>
</div>
</div>
</div>
</div>

<div class="col-md-6">
  <div class="con-com">
    <div class="ficha-organizacion">
      <img src="images/Maria_Luisa_de_Contes_RENAULT_dc610dcc00.jpg" alt="">
      <div class="ficha">
      <!-- <h4 class="con-tit-top-o">Presidente</h4> -->
      <h5>Dª María Luisa DE CONTES D'ESGRANGES</h5> <p>Directora Jurídica - RENAULT ESPAÑA, S.A.</p>
    </div>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/MoLUZVAL_a013f11931.png" alt="">
    <div class="ficha">
    <h5>Sra. Dª Mari Luz DEL VAL</h5> <p>Directora - ATALANTE ARS, S.L.</p>
  </div>
</div>
</div>
</div>


<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
  <img src="images/DARCEAUX_WEB_3b8a7e87ac.png" alt="">
  <div class="ficha">
  <h5>Sr. D. Boris DARCEAUX</h5> <p>Director General para España y Portugal AIR FRANCE KLM</p>
</div>
</div>
</div>
</div>

<div class="col-md-6">
	<div class="con-com">
		<div class="ficha-organizacion">
			<img src="images/SMI_LOWDEF_003_8e7eb0d5b8.jpg" alt="">
			<div class="ficha">
				<h5>Sr. D. Jean-Charles DELGADO</h5> <p>Director General de ACCORHOTELS</p>
			</div>
		</div>
	</div>
</div>


<div class="col-md-6">
  <div class="con-com">
    <div class="ficha-organizacion">
      <img src="images/DUARTE_TARGO_2_acd9deb6a6.jpg" alt="">
      <div class="ficha">
      <h5>Sr. D. Rafael DUARTE</h5> <p>Director de Relaciones Intenacionales Bancarias - BANKINTER</p>
    </div>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/De_Esteban_WEB_5b609098a6.png" alt="">
    <div class="ficha">
    <h5>Sr. D. Francisco DE ESTEBAN</h5> <p>Delegado de La Chambre en Canarias</p>
  </div>
</div>
</div>
</div>


<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
  <img src="images/freval_1_0c8bb4091e.jpg" alt="">
  <div class="ficha">
  <h5>Sra. Dª Myriam FREVAL</h5> <p>Presidenta - ATHOL ASSET</p><span>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
<img src="images/Alfonso_Gajate_Islalink_a88c4f2eca.jpg" alt="">
<div class="ficha">
<h5>Sr. D. Alfonso GAJATE</h5> <p>Presidente ELLALINK</p><span>
</div>
</div>
</div>
</div>
<div class="col-md-6">
  <div class="con-com">
    <div class="ficha-organizacion">
      <img src="images/EVA_IVARS_f068307920.jpg" alt="">
      <div class="ficha">
      <h5>Sra. Dña. Eva IVARS BUIGUES</h5> <p>Directora General ALAIN AFFLELOU ESPAÑA, S.A.U.</p><span>
    </div>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/Nicolas-Loupy_2_5fc6e9c9e3.jpg" alt="">
    <div class="ficha">
    <h5>Sr. D. Nicolas LOUPY</h5> <p>Managing Director - DASSAULT SYSTEMES IBERIA</p><span>
  </div>
</div>
</div>
</div>


<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
  <img src="images/_MG_6053b_27271d61da.jpg" alt="">
  <div class="ficha">
  <h5>Sr. D. Emmanuel MIELVAQUE</h5> <p>EMPLOYENEURS ESPAÑA SL</p><span>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
<img src="images/ocana_2_00eedd1d7a.jpg" alt="">
<div class="ficha">
<h5>Sr. D. Julián OCAÑA</h5> <p>Director - GEDETEC</p><span>
</div>
</div>
</div>
</div>
<div class="col-md-6">
  <div class="con-com">
    <div class="ficha-organizacion">
      <img src="images/Loreto_Ordonez_2013_6be0d5cd2c.jpg" alt="">
      <div class="ficha">
      <h5>Sra. Dña. Loreto ORDOÑEZ</h5> <p>Consejera Delegada - ENGIE</p><span>
    </div>
  </div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/dsanfelipe_5_b9a15108db.jpg" alt="">
    <div class="ficha">
    <h5>Sr. D. Domingo SAN FELIPE</h5> <p></p><span>
  </div>
</div>
</div>
</div>


<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
  <img src="images/LOURDES_bed4dce890.png" alt="">
  <div class="ficha">
  <h5>Sra. Dª Lourdes SÁNCHEZ-CERVERA</h5> <p>Socia Directora - INTERLINCO SERVICIOS LINGUISTICOS Y DE COMUNICACION, S.L.</p><span>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
<img src="images/J.See1_8390430bdf.jpg" alt="">
<div class="ficha">
<h5>Sra. Dª Joëlle SEE</h5> <p>Partner - TeamOn</p><span>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
  <div class="ficha-organizacion">
    <img src="images/christophe_SOUGEY_cb1f88df4e.jpg" alt="">
    <div class="ficha">
    <h5>Sr. D. Christophe SOUGEY</h5> <p>CEO - AREVALO REFRIGERACIÓN MODULAR, S.L.U. </p><span>
  </div>
</div>
</div>
</div>


<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
  <img src="images/RetratosJavierRafael-19_265bbbe364.jpg" alt="">
  <div class="ficha">
  <h5>Sr. D. Javier TABERNERO DA VEIGA</h5> <p>Director General de PROSEGUR COMPAÑIA DE SEGURIDAD S.A.</p>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="con-com">
<div class="ficha-organizacion">
<img src="images/J_Robles_5234_da7d3c71da.jpg" alt="">
<div class="ficha">
	<h5>Sra. Dña. Nathalie VALVERDE</h5> <p>Directora de Relaciones Internacionales - TARGOBANK, S.A.U.</p>
</div>
</div>
</div>
</div>



</div>






    </div>

    </div>
	</section>
@include('partials.homeBoletin')


@endsection
