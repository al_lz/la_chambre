@extends('app')

@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
											<div class="v3-top-ri">
													<ul>
														<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																@auth
																		<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
																@endauth
																@guest
																		<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
																@endguest


													</ul>
											</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>
<section class="inn-page-bg">
		<div class="container">
			<div class="row">
				<div class="inn-pag-ban">
					<h2>Mensaje del presidente</h2>
					<!-- <h5>Grow your business by getting relevant and verified leads</h5> </div> -->
			</div>
		</div>
	</section>
	<section class="p-about com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
          <div class="com-padd">
            <h3>Mensaje del presidente D. Laurent Paillassot</h3>
          </div>
          <div class="csc-textpic-imagewrap" data-csc-images="1" data-csc-cols="2">
            <figure class="csc-textpic-image csc-textpic-last">
              <img src="images/Laurent_Paillassot_big.jpg" width="320" height="434" alt="">
            </figure>
          </div>
					<div class="mensaje pad-bot-red-40 justify">
						<p>El Consejo de Administración del pasado martes 4 de julio, me ha confiado la Presidencia de la Cámara Franco-Española sucediendo a Domingo San Felipe. Bajo su mandato La Chambre ha vivido un fuerte crecimiento en muchos aspectos como por ejemplo el número de socios, número de delegaciones y los acuerdos de colaboración con otras instituciones.</p>
            <p>Para aquellos que me conozcan poco, quisiera comentaros mi trayectoria profesional: he hecho toda mi carrera en el sector financiero en varias instituciones francesas y estadounidenses. He sido hasta 2014 Director General Delegado de Crédit Lyonnais, fecha en la cual me incorporé al Grupo Orange como Director General adjunto. Estoy en España desde 2016 como Consejero Delegado de Orange.</p>
            <p>Recojo con ilusión y responsabilidad el testigo de manos de Domingo San Felipe y mi deseo es continuar en esta misma línea, manteniendo vivo el espíritu de La Chambre de escuchar las casuísticas de todas las empresas (sean start-ups, PyMEs, grandes empresas o multinacionales), y compartiendo con total transparencia nuestros éxitos y nuestros resultados.</p>
            <p>Estamos ahora en un momento importante de cambios en Europa donde más que nunca es necesario estrechar todavía más los lazos entre Francia y España. Tengo la firme convicción de que La Chambre puede jugar un papel fundamental en este momento, en un contexto económico y empresarial favorable al crecimiento, manteniéndose fiel a su misión de promover dicho desarrollo económico y empresarial ante las instituciones públicas de ambos países. Es una tarea para la que sé que contaré con el apoyo del Consejo de Administración, de los Vice-Presidentes y del equipo de la Chambre liderado por su Director General Bertrand Barthelemy, así como de todos los socios. Es mi deseo que trabajemos todos juntos con espíritu de equipo: sois la esencia de esta Cámara y vuestras opiniones, sugerencias e ideas son muy importantes y os invito a que nos las hagáis llegar. Mis predecesores han dejado La Chambre en una situación envidiable: 10 delegaciones operativas, una base asociativa de más de 600 socios, un centro de negocios con una tasa de ocupación que roza el 100%, una variada cartera de servicios y prestaciones a las empresas. Sobre estas sólidas bases, quiero desarrollar las principales acciones de La Chambre alrededor de 3 ejes:
              <ul>
                <li>Reforzar las relaciones entre socios y estrechar lazos entre los socios y la Chambre</li>
                <li>Asegurar que tenemos siempre la oferta de servicios adecuada para dar respuesta a las necesidades de todas las empresas, socios y no -socios, a las que prestamos nuestro apoyo.</li>
                <li>Optimizar el desempeño global y reforzar la solidez financiera de La Chambre.</li>
              </ul>
              <p>Asimismo, comentaros que ya estamos trabajando con el Consejo de Administración para definir un plan de acción que de soporte a esta visión de desarrollo y que permita que La Chambre sea reconocida por todos como una Cámara de Comercio de referencia. Espero tener la ocasión de reunirme con cada uno de vosotros para compartir esta visión, sabiendo que puedo contar con vuestra colaboración y participación.</p>
              <p>Laurent Paillassot</br>Presidente</p>
            </div>
				</div>
			</div>
		</div>
	</section>
@include('partials.homeBoletin')


@endsection
