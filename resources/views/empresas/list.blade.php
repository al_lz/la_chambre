@extends('app')

@section('content')
<section id="menuresponsive">
    <div class="v3-mob-top-menu">
        <div class="container">
            <div class="row">
                <div class="v3-mob-menu">
                    <div class="v3-mob-m-1">
                        <a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
                    </div>
                    <div class="v3-mob-m-2">
                        <div class="v3-top-ri">
                            <ul>
                              <li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
                                <li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Login</a> </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mob-right-nav" data-wow-duration="0.5s">
        <div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
        <!-- <h5>Business</h5> -->
        <ul class="mob-menu-icon">
          <li><a href="/">Home</a> </li>
          <li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
          @auth
              <li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
          @endauth
          @guest
              <li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
          @endguest
          <li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
          </li>
            <h5>La Chambre</h5>
            <ul>
                <li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
                <li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
                <li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
                <li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
            </ul>
        </ul>
    </div>
</section>
<section class="dir-alp dir-pa-sp-top">
    <div class="container">
        <div class="row">
            <div class="dir-alp-tit">
                <h1>Listado de asociados.</h1>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a> </li>
                    <li class="active"><a href="/empresas">Listado de asociados</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
                <div class="dir-alp-con">
    			           <banner-top  bannergeneral="{{ '/storage/'.$bannertop->imagen }}" linkbannergeneral="{{ $bannertop->link }}" :bannerssector="{{ $bannersSector }}" :bannersdelegation="{{ $bannersDelegation }}"></banner-top>
                </div>
        </div>
        <filter-algolia :selected="false" :destacados="{{ $destacados_sector }}" :generales="{{ $destacados_general }}"></filter-algolia>

    </div>
</section>
@endsection
