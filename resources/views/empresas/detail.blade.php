@extends('app')

@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
												<div class="v3-top-ri">
														<ul>
															<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Login</a> </li>

														</ul>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					<li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>

<section class="pg-list-1">
		<div class="container">
			<div class="row">
				<!-- <h6><span class="">delegación</span></h6> -->
				<div class="pg-list-1-left"> <a href="#"><h3>{{ $empresa->nombre }}</h3></a>
					<h5><span class="area">{{ $empresa->sector->nombre }}</span></h5>
					<div class="list-number pag-p1-phone">
						<p><b>Dirección:</b> {{ $empresa->direccion }} - {{ $empresa->cp }} - {{ $empresa->poblacion.' ('.$empresa->provincia }})</p>
						<p><b>Máximo Responsable:</b> {{ $empresa->contacto }}</p>
					</div>
					<div class="list-number pag-p1-phone">
						<ul>
							<li><i class="fa fa-phone" aria-hidden="true"></i> {{ $empresa->telefono }}</li>
							<li><i class="fa fa-envelope" aria-hidden="true"></i> {{ $empresa->email }} </li>
							<li><i class="fa fa-laptop" aria-hidden="true"></i> {{ $empresa->web }} </li>
						</ul>
					</div>

					<div class="list-enqu-btn">
						<ul>
							<li><a href="#" @click.prevent="$modal.show('contacto', { client: {{$empresa}} })" data-dismiss="modal" data-toggle="modal" data-target="#list-quo"> Contactar</a> </li>
						</ul>
					</div>

				</div>
				<div class="pg-list-1-right">
					<div class="list-enqu-btn">
							<div class="list-pg-inn-sp">
								<div class="list-pg-map">
									@if ($empresa->fotos)
											<!--LISTING DETAILS: LEFT PART 2-->
											<div class="pglist-p2 pglist-bg pglist-p-com" id="ld-ser">
													<div class="pglist-p-com-ti">
													<div class="list-pg-inn-sp">
														@foreach (json_decode($empresa->fotos) as $foto)
														<img src="{{ asset('storage/'.$foto) }}" alt="tech" width="100%">
														@endforeach
													</div>
											</div>
											<!--END LISTING DETAILS: LEFT PART 2-->

									@endif
												@if($array_coord)
									{{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6290413.804893654!2d-93.99620524741552!3d39.66116578737809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880b2d386f6e2619%3A0x7f15825064115956!2sIllinois%2C+USA!5e0!3m2!1sen!2sin!4v1469954001005" allowfullscreen=""></iframe> --}}
												<iframe
														width="300"
														height="300"
														frameborder="0"
														scrolling="no"
														marginheight="0"
														marginwidth="0"
														src="https://maps.google.com/maps?q={{ $array_coord[0] }},{{ $array_coord[1] }}&hl=es&amp;z=15&amp;output=embed"
												>
												</iframe>
													@endif
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
    </section>

    <section class="list-pg-bg">
            <div class="container">
                <div class="row">
                    <div class="com-padd">
                        <div class="list-pg-lt list-page-com-p">
                            <!--LISTING DETAILS: LEFT PART 1-->
                            <div class="pglist-p1 pglist-bg pglist-p-com" id="ld-abour">
                                <div class="pglist-p-com-ti">
                                    <h3><span>Servicios</span></h3></div>
                                <div class="list-pg-inn-sp">
                                    <p>{!! $empresa->descripcion !!}</p>
                                </div>
                            </div>
                            <!--END LISTING DETAILS: LEFT PART 1-->


                            <!--LISTING DETAILS: LEFT PART 2-->
                            <div class="pglist-p2 pglist-bg pglist-p-com" id="ld-ser">
                                <div class="pglist-p-com-ti">
                                    <h3><span>Información</h3> </div>
                                <div class="list-pg-inn-sp">
                                    <p>{!! $empresa->servicios !!} </p>
                                </div>
                            </div>
                            <!--END LISTING DETAILS: LEFT PART 2-->


                            @if ($empresa->fotos)

                                <!--LISTING DETAILS: LEFT PART 2-->
                                <div class="pglist-p2 pglist-bg pglist-p-com" id="ld-ser">
                                    <div class="pglist-p-com-ti">
                                        <h3><span>Fotos</h3> </div>
                                    <div class="list-pg-inn-sp">
                                        <carousel>
                                                @foreach (json_decode($empresa->fotos) as $foto)
                                                    <img src="{{ asset('storage/'.$foto) }}" alt="tech">

                                                @endforeach

                                            </carousel>
                                    </div>
                                </div>
                                <!--END LISTING DETAILS: LEFT PART 2-->

                            @endif

                        </div>
                        <div class="list-pg-rt">
                            <!--LISTING DETAILS: LEFT PART 10-->
                            <div class="list-mig-like">
                                <div class="list-ri-spec-tit">
                                    @if ($bannerdetail)
                                    <!-- <a href="#!">
                                        <div class="list-mig-like-com">
                                            <div class="list-mig-lc-img">
                                                    <img src="{{ asset('storage/'.$bannerdetail->imagen ) }}" alt="">

                                                </div>
                                                <div class="list-mig-lc-con">
                                                </div>
                                            </div>
                                        </a> -->
                                    @endif
                                    {{-- @php
                                        dd($bannerdetailsector->first()->imagen)
                                    @endphp --}}
                                    @if ($bannerdetailsector->count() > 0)

                                        <a href="#!">
                                            <div class="list-mig-like-com">
												<div class="list-mig-lc-img">
														<a href="{{ $bannerdetailsector->first()->link }}">
																<img src="{{ asset('storage/'.$bannerdetailsector->first()->imagen ) }}" alt="">
															</a>
													{{-- <img src="{{ asset('storage/'.$bannerdetailsector->first()->imagen ) }}" alt="">  --}}

												</div>
                                                <div class="list-mig-lc-con">
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                    @if ($bannerdetaildelegation && $bannerdetaildelegation->count() > 0)

                                        <a href="#!">
                                            <div class="list-mig-like-com">
                                                <div class="list-mig-lc-img">
													<a href="{{ $bannerdetaildelegation->first()->link }}">

														<img src="{{ asset('storage/'.$bannerdetaildelegation->first()->imagen ) }}" alt="">
													</a>

												</div>
                                                <div class="list-mig-lc-con">
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                            </div>
                            <!--END LISTING DETAILS: LEFT PART 10-->
                        </div>

                            <!--END LISTING DETAILS: LEFT PART 8-->
                            <!--LISTING DETAILS: LEFT PART 9-->

                    </div>
                </div>
            </div>
        </div></section>

@endsection
