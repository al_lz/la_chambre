@extends('app')
@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
												<div class="v3-top-ri">
														<ul>
															<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																	@auth
																			<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
																	@endauth
																	@guest
																			<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
																	@endguest


														</ul>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					<li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>



<section class="inn-page-bg">
		<div class="container">
			<div class="row">
				<div class="inn-pag-ban">
					<h2>Quiénes somos</h2>
					<!-- <h5>Grow your business by getting relevant and verified leads</h5> </div> -->
			</div>
		</div>
	</section>
	<section class="p-about com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
          <div class="com-padd">
            <!-- <h3>Mensaje del presidente D. Laurent Paillassot</h3> -->
          </div>
          <div class="csc-textpic-imagewrap" data-csc-images="1" data-csc-cols="2">
            <figure class="csc-textpic-image csc-textpic-last">
              <!-- <img src="images/Laurent_Paillassot_big.jpg" width="320" height="434" alt=""> -->
            </figure>
          </div>
					<div class="mensaje pad-bot-red-40 justify">
						<p>Creada en 1894, la Cámara Franco-Española de Comercio e Industria, La Chambre es una asociación de empresas con sede en Madrid y delegaciones en Bilbao, Islas Baleares, Islas Canarias, Málaga, Sevilla, Valencia, Valladolid, Vigo y Zaragoza. La Chambre tiene una doble vocación: Club de negocios y proveedor de servicios.</p>
              <ul>
                <li>Colabora desde 1894 con las instituciones francesas y españolas con el apoyo y la confianza de las empresas y emprendedores.</li>
								<li>Es un referente y una plataforma de contactos para las empresas francesas y españolas.</li>
								<li>-	Ofrece a las empresas una amplia gama de prestaciones y servicios que abarcan las diferentes fases de implantación y desarrollo en ambos mercados.</li>
              </ul>
            </div>


				</div>
			</div>
		</div>
	</section>
@include('partials.homeBoletin')


@endsection
