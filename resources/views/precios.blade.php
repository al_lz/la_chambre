@extends('app')

@section('content')
<section id="menuresponsive">
    <div class="v3-mob-top-menu">
        <div class="container">
            <div class="row">
                <div class="v3-mob-menu">
                    <div class="v3-mob-m-1">
                        <a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
                    </div>
                    <div class="v3-mob-m-2">
                      <div class="v3-top-ri">
                          <ul>
                            <li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
                                @auth
                                    <li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
                                @endauth
                                @guest
                                    <li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
                                @endguest


                          </ul>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mob-right-nav" data-wow-duration="0.5s">
        <div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
        <!-- <h5>Business</h5> -->
        <ul class="mob-menu-icon">
          <li><a href="/">Home</a> </li>
          <li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
          @auth
              <li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
          @endauth
          @guest
              <li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
          @endguest
          <li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
          </li>
            <h5>La Chambre</h5>
            <ul>
                <li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
                <li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
                <li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
                <li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
            </ul>
        </ul>
    </div>
</section>
<section class="dir-pa-sp-top dir-pa-sp-top-bg v4-pri-bg">
    <div class="rows">
        <div class="container">
            <div class="v4-price-list com-padd">
              <h2>ANUARIO DIGITAL DE LOS SOCIOS 2018</h2>
              <p>Este año se desarrolla por primera vez una <strong>versión digital</strong> del tradicional anuario de la Chambre, de forma que estará <strong>siempre actualizado</strong> y permitirá optimizar
                las búsquedas mediante la aplicación de filtros por actividad, sector, etc. y contactar directamente con las más de 600 empresas asociadas.</p>
                <p>No se pierdan la oportunidad de </strong>incluir publicidad<strong> en este nuevo anuario digital, que llegará a unos 1.400 contactos, entre empresas, instituciones y organismos. La mejor manera de diferenciarse y obtener visibilidad extra entre los socios y la comunidad de negocios que agrupamos.</p>
                <div class="com-padd">
                  <h3>TARIFAS DE PUBLICIDAD ANUAL (IVA no incluido)</h3>
                  <p></p>
                  <p></p>
              </div>
                <div class="col-md-3">
                    <div class="v3-pril-inn">
                        <div class="v4-pril-inn-top">
                            <h2>Banner en todas las páginas</h2>
                            <p class="v4-pril-price"><b>1500</b> <span class="v4-pril-curr">€</span> </p>
                        </div>
                        <div class="v4-pril-inn-bot">
                            <ul>
                            <li><i class="fa fa-check"></i>La inclusión de un banner en todas las páginas del sitio web le permitirá tener presencia continuada en todas las visitas al anuario.</li>
                            <!-- <li><i class="fa fa-check"></i>característica </li> -->
                            </ul>
                            <a class="waves-effect waves-light btn-large full-btn" href="/contacto">Consultar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="v4-pril-inn">
                        <div class="v4-pri-best">Más popular</div>
                        <div class="v4-pril-inn-top">
                            <h2>Banner central</h2>
                            <p class="v4-pril-price"><b>900</b> <span class="v4-pril-curr">€</span> </p>
                        </div>
                        <div class="v4-pril-inn-bot">
                            <ul>
                            <li><i class="fa fa-check"></i>El banner central supone un espacio privilegiado en la zona más visible del sitio web.</li>
                            <li><i class="fa fa-check"></i>Tamaño 1500 X 500 px  </li>
                            </ul>
                            <a class="waves-effect waves-light btn-large full-btn" href="/contacto">Consultar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="v4-pril-inn v4-pril-inn-bot-0">
                        <div class="v4-pril-inn-top">
                            <h2>Banner top</h2>
                            <p class="v4-pril-price"><b>700</b> <span class="v4-pril-curr">€</span> </p>
                        </div>
                        <div class="v4-pril-inn-bot">
                            <ul>
                            <li><i class="fa fa-check"></i>Tras el banner central, el banner top es el espacio con mayor visibilidad en la web.</li>
                            <li><i class="fa fa-check"></i>Tamaño 815 X 140px</li>
                            </ul>
                            <a class="waves-effect waves-light btn-large full-btn" href="db-listing-add.html">Consultar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="v4-pril-inn v4-pril-inn-bot-0">
                        <div class="v4-pril-inn-top">
                            <h2>Banner lateral</h2>
                            <p class="v4-pril-price"><b>550</b> <span class="v4-pril-curr">€</span> </p>
                        </div>
                        <div class="v4-pril-inn-bot">
                            <ul>
                            <li><i class="fa fa-check"></i>El banner lateral se sitúa a un lado u otro del sitio web junto a la zona de consulta de la información.</li>
                            <li><i class="fa fa-check"></i>Tamaño 300 X 300px </li>
                            </ul>
                            <a class="waves-effect waves-light btn-large full-btn" href="/contacto">Consultar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="com-padd">
              <h3>Aparecer como destacados en las búsquedas del anuario</h3>
              <p>Primer, segundo o tercer lugar en su sector, con color destacado, independientemente de su nombre (las empresas aparecen por orden alfabético dentro de cada sector) 150, 100 o 50 €+IVA respectivamente.
                <p>Primer, segundo o tercer lugar en la lista completa de asociados, con color destacado, antes de filtrar por sector, 300, 250 o 200 €+IVA respectivamente.</p>
              </div>
              <div class="com-padd">
                <h3>Logotipos</h3>
                <p>El importe por insertar el logotipo de la empresa en el listado de asociados será de 30€</p>
                <p>Por la contratación de publicidad, se insertará además su logotipo gratuitamente, así como los logotipos de las empresas pertenecientes al Club d’Affaires France-Espagne, que se incluyen siempre en el anuario.</p>
            </div>
            <div class="com-padd">
              <!-- <p>Texto oferta</p> -->
          </div>
        </div>
    </div>
</section>

@include('partials.homeBoletin')


@endsection
