<contactar inline-template>

    <modal name="contacto" :height="'auto'" @before-open="beforeOpen">
    
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dir-pop-head">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title" v-text="'Contactar con '+form.client.nombre">Contacto</h4>
                        <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                    </div>
                    <div class="modal-body dir-pop-body">
                        <form method="post" class="form-horizontal" @submit.prevent="contactar">
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Nombre y apellidos *</label>
                                <div class="col-md-8">
                                    <input class="form-control" v-model="form.name" name="fname" placeholder="" required="" type="text"> 
                                </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Teléfono</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="mobile" v-model="form.tel" placeholder="" type="text"> 
                                </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="email" v-model="form.email" placeholder="" type="text"> 
                                </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Mensaje</label>
                                <div class="col-md-8 get-quo">
                                    <textarea class="form-control" v-model="form.message" ></textarea>
                                </div>
                            </div>
                            <div class="form-group has-feedback ak-field" v-if="error">
                                    <label class="col-md-4 control-label">Errores</label>
                                    <div class="col-md-8 get-quo">
                                        <p v-for="result in error.errors" v-text="result"></p>
                                        {{-- <textarea class="form-control" v-text="error.errors" ></textarea> --}}
                                    </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <div class="col-md-6 col-md-offset-4">
                                        <button class="pop-btn" type="submit">
                                                <i v-if="enviando" class="fa fa-spinner fa-spin"></i>Enviar
                                              </button>
                                    {{-- <input value="Enviar" class="pop-btn" type="submit"> </div> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </modal>
</contactar>