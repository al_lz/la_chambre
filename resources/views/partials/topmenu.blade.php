<section id="menuprincipal">
    <div class="v3-top-menu">
        <div class="container">
            <div class="row">
                <div class="v3-menu">
                    <div class="v3-m-1">
                        <a href="/"><img src="{{ asset('/images/lachambre.png')}}" alt="logo"> </a>
                    </div>
                    <div class="v3-m-2">
                        <ul>
                            <li><a class="" href="/" data-activates="drop-menu-home">Home</a></li>
                            <li><a class="" href="/empresas" data-activates="drop-mega-dash">Empresas</a>
                            <li><a class="dropdown-button ed-sub-menu" href="#" data-activates="drop-mega-menu">La Chambre</a>
                                <ul id="drop-mega-menu" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 6.5px; left: 335px; opacity: 1; display: none;">
                                    <li><a href="/quienes-somos"><i class="fa fa-list-ul"></i> Quiénes somos</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/organizacion"><i class="fa fa-plus"></i> Organización</a></li>
                                    <li class="divider"></li>
                                    <li><a class="" href="/clubdaffaires" data-activates="drop-mega-dash"><i class="fa  fa-life-ring"></i> Club d'Affaires</a> </li><li class="divider"></li>
                                    <li><a class="" href="/mensajepresidente" data-activates="drop-mega-dash"><i class="fa  fa-comment"></i> Mensaje del Presidente</a> </li>
                                </ul>
                          </li>
                            @auth
                                <li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
                            @endauth
                            @guest
                                <li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
                            @endguest
                            <li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a></li>
                            @auth
                                <li><a class="" href="/chat" data-activates="drop-menu-page" style="padding-right: 0;">Mensajes
                                        @if ( ! \Request::is('chat'))
                                            <totalmessages :user="{{ auth()->user() }}"></totalmessages>
                                        @endif
                                    </a> </li>
                            @endauth
                        </ul>
                    </div>
                    <div class="v3-m-3">
                        <div class="v3-top-ri">
                            <ul>
                              @auth
                                  <li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
                              @endauth
                              @guest
                                  <li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
                              @endguest
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="all-drop-down-menu">
                    <!--DROP DOWN MENU: HOME-->
                    <!--END DROP DOWN MENU-->

                </div>
            </div>
        </div>
    </div>
</section>
