
<contactarlachambre inline-template>

<div class="con-com">
    <h4 class="con-tit-top-o">Sede Central</h4>
    <p>Calle Capitán Haya 38 - 4ª, 28020 Madrid</p> <span class="icos"><i class="material-icons">phone</i> Tel: + 34 913 072 100</span> <span class="icos"><i class="material-icons">email</i> Email: lachambre@lachambre.es</span>

    <div class="con-page">
        <div class="cpn-pag-form">
            <form method="post" class="doblesrazones" @submit.prevent="contactar">
                <div>
                    <div class="input-field col s12">
                        <input id="gfc_name"  v-model="form.name"  type="text" class="validate" required="">
                        <label for="gfc_name">Nombre y apellidos</label>
                    </div>
                </div>
                <div>
                    <div class="input-field col s12">
                        <input id="gfc_mob" type="number" class="validate" v-model="form.tel" >
                        <label for="gfc_mob">Tfno</label>
                    </div>
                </div>
                <div>
                    <div class="input-field col s12">
                        <input id="gfc_mail" type="email" class="validate" v-model="form.email" >
                        <label for="gfc_mail">Email</label>
                    </div>
                </div>
                <div>
                    <div class="input-field col s12">
                        <textarea id="gfc_msg" class="validate" v-model="form.message" ></textarea>
                        <label for="gfc_msg">Introduzca se mensaje</label>
                    </div>
                </div>
                <div>
                    <div class="input-field col s12">
                        <i class="waves-effect waves-light btn-large full-btn list-red-btn waves-input-wrapper" style="">
                        <input type="submit" value="Enviar" class="waves-button-input"></i> 
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

</contactarlachambre>