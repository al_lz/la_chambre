    <!--FIND YOUR SERVICE-->
    <section class="cat-v2-hom com-padd mar-bot-red-m30">
        <div class="container">
            <div class="row">
                <div class="com-title">
                    <h2>Encuentra servicios por categoría</h2>
                    <p>Explora negocios entre nuestros partners y filtra tu búsqueda por categoría.</p>
                </div>
                <div class="cat-v2-hom-list">
                    <ul>
                        @foreach ($sectors as $sector)
                            <li>
                              <a href="/empresassector/{{ $sector->id."-".$sector->nombre }}"><img src="{{ asset('storage/'.$sector->imagen ) }}" class="imagenicon" alt="">
                                <p>{{ $sector->nombre }}</p>
                            </a>
                            </li>
                        @endforeach



                    </ul>
                </div>
            </div>
        </div>
    </section>
