<section class="com-padd com-padd-redu-top">
    <div class="container">
        <div class="row">
            <div class="com-title">
                <h2>Explora por delegación</h2>
                <p>Encuentra las empresas de tu ciudad para conectar con ellos.</p>
            </div>
            @foreach ($deletations as $item)
            <div class="col-md-3">
                <a href="/empresasdelegation/{{ $item->id."-".$item->nombre }}">
                    <div class="list-mig-like-com">
                        <div class="list-mig-lc-img"> <img src="{{ asset('storage/'.$item->image ) }}" alt=""> </div>
                        <div class="list-mig-lc-con list-mig-lc-con2">
                        <h5>{{ $item->nombre}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach

        </div>
    </div>
</section>
