

    <!--FOOTER SECTION-->
    <footer id="colophon" class="site-footer clearfix">
        <div id="quaternary" class="sidebar-container " role="complementary">
            <div class="sidebar-inner">
                <div class="widget-area clearfix">
                    <div id="azh_widget-2" class="widget widget_azh_widget">
                        <div data-section="section">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 foot-logo"> <img src="{{ asset('/images/lachambre.png')}}" alt="logo">
                                        <h4>Soporte y ayuda</h4>
                                        <ul>
                                            <li> <a href="/login">Login</a> </li>
                                            <li> <a href="/quienes-somos">Nosotros</a> </li>
                                            <li> <a href="/contacto">Anúnciate</a> </li>
                                        </ul>

                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <h4>Sectores</h4>
                                        <ul class="two-columns">
                                            @foreach ($sectoresfoot as $sector)

                                            <li> <a href="/empresassector/{{ $sector->id."-".$sector->nombre }}">{{ $sector->nombre }}</a> </li>
                                            @endforeach

                                        </ul>


                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <h4>Delegaciones</h4>
                                        <ul>
                                                @foreach ($delegationsfoot as $item)

                                                <li> <a href="/empresasdelegation/{{ $item->id."-".$item->nombre }}">{{ $item->nombre }}</a> </li>
                                                @endforeach

                                            </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-section="section" class="foot-sec2">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-5 foot-social">
                                        <h4>Síganos en Redes Sociales</h4>
                                        <ul>
                                            <li><a href="https://plus.google.com/+C%C3%A1maraFrancoEspa%C3%B1ola?hl=es"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
                                            <li><a href="https://twitter.com/LachambreEsp" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                            <li><a href="http://www.linkedin.com/groups?home=&gid=3008807&trk=anet_ug_hm&goback=.mid_I183252754*4140_*1.anb_3008807_*2_*1_*1_*1_*1_*1" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                                            <li><a href="https://www.youtube.com/channel/UCecRuiEYohKg7VMpLAW8hSQ" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                            <li><a href="https://www.flickr.com/photos/122743756@N08/sets/" target="_blank"><i class="fa fa-flickr" aria-hidden="true"></i></a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .widget-area -->
            </div>
            <!-- .sidebar-inner -->
        </div>
        <!-- #quaternary -->
    </footer>
    <!--COPY RIGHTS-->
    <section class="copy">
        <div class="container">
            <p>copyrights © 2018 La Chambre &nbsp;&nbsp;All rights reserved. By <a href="https://sirope.es" target="_blank">Sirope</a></p>
        </div>
    </section>
