<section class="web-app com-padd">
    <div class="container">
        <div class="row">
            <div class="col-md-6 web-app-img"> <img src="/images/banner.jpg" alt=""> </div>
            <div class="col-md-6 web-app-con">
                <h2>¿Quiere conectar con más de 600 empresas?</h2>
                <ul>
                    <li><i class="fa fa-check" aria-hidden="true"></i> Participe en nuestras actividades y eventos.</li>
                    <li><i class="fa fa-check" aria-hidden="true"></i> Disfrute de ventajas y servicios preferentes.</li>
                    <li><i class="fa fa-check" aria-hidden="true"></i> Apóyese en nuestra estructura para desarrollar su negocio.</li>
                    <li><i class="fa fa-check" aria-hidden="true"></i> Comparta sus ambiciones, inquietudes y oportunidades de intercambio.</li>
                  </br>
                    <li><a href="/contacto" class="waves-effect waves-light btn">Solicite su alta como socio</a></li>
                <!-- <form>
                    <ul>
                        <li>
                             </li>
                        <li>
                            <input placeholder="Introduzca su email" type="text"> </li>
                        <li>
                            <input value="Suscribir al boletín" type="submit"> </li>
                    </ul>
                </form>
                        <div class="row">
                            <div class="col s12 hom-cr-acc-check">
                                <input id="test5" type="checkbox">
                                <label for="test5">Acepto las condiciones de la política de privacidad y también que se me envíen notificaciones comerciales..</label>
                            </div>
            </div> -->
        </div>
    </div>
</div>
</section>
