@component('mail::message')
# Nuevos mensajes

Hola *{{ $user->name   }}* tienes *{{ $user->unread }}* mensajes sin leer en el chat de La Chambre.
   

@component('mail::button', ['url' => env('APP_URL').'/chat' ])
Leer mensajes
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
