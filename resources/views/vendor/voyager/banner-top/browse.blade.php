@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        {{-- @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
        @if(isset($dataType->order_column) && isset($dataType->order_display_column))
            <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
            </a>
        @endif
        @endcan --}}
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">   
        @include('voyager::alerts')
        
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <h3 class="panel-title">Por Seccion</h3>
                    </div>
                    <div class="panel-body">
                            <div class="table-responsive">
                                    <table id="dataTable2" class="table table-hover">
                                        <thead>
                                            <tr>
                                               
                                                <th>
                                                   Sector
                                                </th>
                                                <th>
                                                    Banner
                                                </th>
                                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>GENERAL</td>
                                                <td><img src="{{ Voyager::image( $general->imagen ) }}" alt="general" style="width:100px" ></td>
                                                <td class="no-sort no-click" id="bread-actions">
                                                        {{-- <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $general->id }}" id="delete-{{ $general->id }}">
                                                                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                                                </a> --}}
                                                                <a href="{{ config('app.url') }}/admin/banner-top/{{ $general->id }}/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                                <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                                </a>
                                                                
                                                </td>
                                            </tr>
                                            @foreach($sectores as $sector)
                                            <tr>
                                            <td>{{ $sector->nombre }} <span class="badge {{ ($sector->clientes->count() > 0)?'badge-info':'badge-warning'  }}">{{ $sector->clientes->count()  }}</span></td>
                                                <td>@if( $sector->bannertop )
                                                    <img src="@if( $sector->bannertop ){{ Voyager::image( $sector->bannertop->imagen ) }}@else{{ 'nada' }}@endif" style="width:100px">
                                                    @else
                                                    --
                                                    @endif
                                                </td>
                                                <td class="no-sort no-click" id="bread-actions">
                                                        {{-- <span>{{ $sector->bannertop }}</span> --}}
                                                        @if( $sector->bannertop )
                                                            <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $sector->bannertop->id }}" id="delete-{{ $sector->bannertop->id }}">
                                                            <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                                            </a>
                                                            <a href="{{ config('app.url') }}/admin/banner-top/{{ $sector->bannertop->id }}/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                            </a>
                                                            {{-- <a href="{{ config('app.url') }}/admin/banner-top/{{ $sector->bannertop->id }}" title="View" class="btn btn-sm btn-warning pull-right view">
                                                            <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                                                            </a> --}}
                                                        @else
                                                            <a href="{{ config('app.url') }}/admin/banner-top/createWithSector/{{ $sector->id }}" class="btn btn-success btn-add-new pull-right " style="text-decoration:none;">
                                                                <i class="voyager-plus"></i> <span>Add New</span>
                                                            </a>
                                                        @endif
                                                    </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                        </div>
                </div>
            </div>
        </div> <!-- .col-md-6 -->
        <div class="col-md-6">
                <div class="panel panel-bordered">
                        <div class="panel-heading">
                            <h3 class="panel-title">Por Delegacion</h3>
                        </div>
                        <div class="panel-body">
                                <div class="table-responsive">
                                        <table id="dataTable2" class="table table-hover">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>
                                                       Delegacion
                                                    </th>
                                                    <th>
                                                        Banner
                                                    </th>
                                                    <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {{-- <tr>
                                                    <td>GENERAL</td>
                                                    <td><img src="{{ Voyager::image( $general->imagen ) }}" alt="general" style="width:100px" ></td>
                                                    <td class="no-sort no-click" id="bread-actions">
                                                          
                                                                    <a href="{{ config('app.url') }}/admin/banner-detail/{{ $general->id }}/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                                    </a>
                                                                   
                                                    </td>
                                                </tr> --}}
                                                @foreach($delegaciones as $delegacion)
                                                <tr>
                                                    <td>{{ $delegacion->nombre }}</td>
                                                    <td>@if( $delegacion->bannertop )
                                                        <img src="@if( $delegacion->bannertop ){{ Voyager::image( $delegacion->bannertop->imagen ) }}@else{{ 'nada' }}@endif" style="width:100px">
                                                        @else
                                                        --
                                                        @endif
                                                    </td>
                                                    <td class="no-sort no-click" id="bread-actions">
                                                            {{-- <span>{{ $delegacion->bannertop }}</span> --}}
                                                            @if( $delegacion->bannertop )
                                                                <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $delegacion->bannertop->id }}" id="delete-{{ $delegacion->bannertop->id }}">
                                                                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                                                </a>
                                                                <a href="{{ config('app.url') }}/admin/banner-top/{{ $delegacion->bannertop->id }}/edit" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                                <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                                </a>
                                                                {{-- <a href="{{ config('app.url') }}/admin/banner-top/{{ $delegacion->bannertop->id }}" title="View" class="btn btn-sm btn-warning pull-right view">
                                                                <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                                                                </a> --}}
                                                            @else
                                                                <a href="{{ config('app.url') }}/admin/banner-top/createWithDelegation/{{ $delegacion->id }}" class="btn btn-success btn-add-new pull-right " style="text-decoration:none;">
                                                                    <i class="voyager-plus"></i> <span>Add New</span>
                                                                </a>
                                                            @endif
                                                        </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                            </div>
                    </div>
                </div>
        </div>
    </div>
</div> <!-- .page-content -->

    

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
                var table2 = $('#dataTable2').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
@stop
