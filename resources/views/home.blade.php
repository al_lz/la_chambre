@extends('app')

@section('content')

    <section>
        <div class="v3-mob-top-menu">
            <div class="container">
                <div class="row">
                    <div class="v3-mob-menu">
                        <div class="v3-mob-m-1">
                            <a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
                        </div>
                        <div class="v3-mob-m-2">
                            <div class="v3-top-ri">
                                <ul>
                                    <li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Sign In</a> </li>
                                    <li><a href="price.html" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li>
                                    <li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mob-right-nav" data-wow-duration="0.5s">
            <div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
            <h5>Business</h5>
            <ul class="mob-menu-icon">
                <li><a href="price.html">Add Business</a> </li>
                <li><a href="#!" data-toggle="modal" data-target="#register">Register</a> </li>
                <li><a href="#!" data-toggle="modal" data-target="#sign-in">Sign In</a> </li>
            </ul>
            <h5>All Categories</h5>
            <ul>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Help Services</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Appliances Repair &amp; Services</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Furniture Dealers</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Packers and Movers</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Pest Control </a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Solar Product Dealers</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Interior Designers</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Carpenters</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Plumbing Contractors</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Modular Kitchen</a> </li>
                <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Internet Service Providers</a> </li>
            </ul>
        </div>
    </section>
<!--
    <section id="background1" class="dir1-home-head">
        <div class="container dir-ho-t-sp">
            <div class="row">
                <div class="dir-hr1">
                    <div class="dir-ho-t-tit dir-ho-t-tit-2">
                        <h1>Conecta con las empresas asociadas en La Chambre</h1>
                        <p>Networking y el negocio entre nuestros socios.</p>
                    </div>
                        <form class="tourz-search-form">
                            <div class="input-field">
                                <input type="text" id="select-city" class="autocomplete">
                                <label for="select-city">Introduce la delegación</label>
                            </div>
                            <div class="input-field">
                                <input type="text" id="select-search" class="autocomplete">
                                <label for="select-search" class="search-hotel-type">Busca por empresa, sector o actividad</label>
                            </div>
                            <div class="input-field">
                                <input type="submit" value="buscar" class="waves-effect waves-light tourz-sear-btn"> </div>
                        </form>
                </div>
            </div>
        </div>
    </section>
-->
<section class="slider-app com-padd">
            <div class="row">
                <div class="dir-hr1">
                    <div class="dir-ho-t-tit dir-ho-t-tit-2">
                        <h1 style="color: #2a2b33">Anuario digital de La Chambre Subtítulo: Su plataforma de negocios y networking franco-española</h1>
                        <p style="color: #2a2b33">Conecta y haz networking con las empresas asociadas.</p>
                    </div>
                {{-- <div class="slideshow-container">

                      <!-- Full-width images with number and caption text -->
                      <div class="mySlides " style="display: block;">
                        <div class="numbertext">1 / 2</div>
                        <img src="images/air_france_top_banner.jpg" style="width:100%">
                      </div>

                      <div class="mySlides " style="display: none;">
                        <div class="numbertext">2 / 2</div>
                        <img src="images/slide.jpg" style="width:100%">
                      </div>


                      <!-- Next and previous buttons -->
                    <!--
                      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                      <a class="next" onclick="plusSlides(1)">&#10095;</a>
                    -->
                    </div> --}}
                    <carousel>
                        @foreach ($bannersHome as $banner)
                            <img src="{{ asset('storage/'.$banner->imagen) }}" alt="tech">

                        @endforeach
                        {{-- <img src="https://placeimg.com/1000/480/tech?2" alt="tech">
                        <img src="https://placeimg.com/1000/480/tech?3" alt="tech">
                        <img src="https://placeimg.com/1000/480/tech?4" alt="tech"> --}}

                    </carousel>
                    <br>

                    <!-- The dots/circles -->
                    <div style="text-align:center">
                      <span class="dot" onclick="currentSlide(1)"></span>
                      <span class="dot" onclick="currentSlide(2)"></span>
                      <span class="dot" onclick="currentSlide(3)"></span>
                    </div>

                </div>
</div></section>
<!-- Slideshow container -->

@include('partials.homeServicios')

@include('partials.homeDelegaciones')


    <!--EXPLORE CITY LISTING-->

    <!--ADD BUSINESS-->
    <section class="com-padd home-dis">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Promociones aquí su negocio <a href="price.html">ir a tarifas</a></h2> </div>
            </div>
        </div>
    </section>

    <!--MOBILE APP-->


    @include('partials.homeBoletin')

    <!--QUOTS POPUP-->
    <section>
        <!-- GET QUOTES POPUP -->
        <div class="modal fade dir-pop-com" id="list-quo" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dir-pop-head">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Get a Quotes</h4>
                        <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                    </div>
                    <div class="modal-body dir-pop-body">
                        <form method="post" class="form-horizontal">
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Full Name *</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="fname" placeholder="" required="" type="text"> </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Mobile</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="mobile" placeholder="" type="text"> </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="email" placeholder="" type="text"> </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Message</label>
                                <div class="col-md-8 get-quo">
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <div class="col-md-6 col-md-offset-4">
                                    <input value="SUBMIT" class="pop-btn" type="submit"> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- GET QUOTES Popup END -->
    </section>




@endsection
