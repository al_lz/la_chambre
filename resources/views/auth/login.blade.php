@extends('app')

@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
												<div class="v3-top-ri">
														<ul>
															<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																	@auth
																			<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
																	@endauth
																	@guest
																			<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
																	@endguest


														</ul>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					<li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>



<section class="arriba tz-register">
    <div class="log-in-pop">
        <div class="log-in-pop-right">
            <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="">
            </a>
            <h4>Iniciar Sesión</h4>
            <p>Bienvenido de nuevo. Introduce tus datos para acceder a la plataforma.</p>
            <form class="s12 ng-pristine ng-valid" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
            <div class="espaciado">
                    <div class="input-field s12">
                        <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                        <label>Email</label>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

              <div class="espaciado">
                    <div class="input-field s12">
                        <input id="password" type="password" class="validate" name="password" required>
                        <label>Password</label>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <label>Password</label>

                    </div>

                </div>

              <div class="espaciado">

                    <div class="input-field s4">

                        <i class="waves-effect waves-light log-in-btn waves-input-wrapper" style=""><input value="Acceder" class="waves-button-input" type="submit"></i> </div>

                </div>

                <div>

                    <div class="input-field s12"> <a href="{{ route('password.request') }}">¿Has olvidado la contraseña?</a></div>

                </div>

            </form>

        </div>

    </div>

</section>
@include('partials.homeBoletin')

@endsection
