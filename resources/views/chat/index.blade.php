@extends('app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    @auth
                    <div class="card-header">Mensajes</div>

                    <div class="card-body" id="chat-app">
                        <chat-app :user="{{ auth()->user() }}"></chat-app>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>
@endsection
