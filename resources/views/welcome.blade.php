<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src=“https://www.googletagmanager.com/gtag/js?id=UA-117832205-2“></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag(‘js’, new Date());

         gtag(‘config’, ‘UA-117832205-2’);
        </script>



        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="overflow: visible;">
        <!--PRE LOADING-->
        <div id="preloader" style="display: none;">
            <div id="status" style="display: none;">&nbsp;</div>
        </div>
        <!--BANNER AND SERACH BOX-->
        <section>
            <div class="v3-top-menu">
                <div class="container">
                    <div class="row">
                        <div class="v3-menu">
                            <div class="v3-m-1">
                                <a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
                            </div>
                            <div class="v3-m-2">
                                <ul>
                                    <li><a class="" href="#" data-activates="drop-menu-home">Home</a>
                                    </li>
                                    <li><a class="dropdown-button ed-sub-menu" href="#" data-activates="drop-mega-menu">Empresas</a><ul id="drop-mega-menu" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 6.5px; left: 335px; opacity: 1; display: none;">
                                <li><a href="list.html"><i class="fa fa-list-ul"></i> Ver todas</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="add-listing.html"><i class="fa fa-plus"></i> Solicitar alta</a>
                                </li>

                            </ul>
                                    </li>
                                    <li><a class="" href="#" data-activates="drop-mega-dash">Beneficios</a>
                                    </li>
                                    <li><a class="" href="#" data-activates="drop-menu-page">Anúnciate</a>
                                    </li>
                                    <li><a class="" href="#" data-activates="drop-menu-admin">Contacto</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="v3-m-3">
                                <div class="v3-top-ri">
                                    <ul>
                                        <li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
    <!-- 									<li><a href="db-listing-add.html" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="all-drop-down-menu">
                            <!--DROP DOWN MENU: HOME-->
                            <!--END DROP DOWN MENU-->

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="v3-mob-top-menu">
                <div class="container">
                    <div class="row">
                        <div class="v3-mob-menu">
                            <div class="v3-mob-m-1">
                                <a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
                            </div>
                            <div class="v3-mob-m-2">
                                <div class="v3-top-ri">
                                    <ul>
                                        <li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Sign In</a> </li>
                                        <li><a href="price.html" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li>
                                        <li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mob-right-nav" data-wow-duration="0.5s">
                <div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
                <h5>Business</h5>
                <ul class="mob-menu-icon">
                    <li><a href="price.html">Add Business</a> </li>
                    <li><a href="#!" data-toggle="modal" data-target="#register">Register</a> </li>
                    <li><a href="#!" data-toggle="modal" data-target="#sign-in">Sign In</a> </li>
                </ul>
                <h5>All Categories</h5>
                <ul>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Help Services</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Appliances Repair &amp; Services</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Furniture Dealers</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Packers and Movers</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Pest Control </a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Solar Product Dealers</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Interior Designers</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Carpenters</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Plumbing Contractors</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Modular Kitchen</a> </li>
                    <li><a href="list.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Internet Service Providers</a> </li>
                </ul>
            </div>
        </section>
    <!--
        <section id="background1" class="dir1-home-head">
            <div class="container dir-ho-t-sp">
                <div class="row">
                    <div class="dir-hr1">
                        <div class="dir-ho-t-tit dir-ho-t-tit-2">
                            <h1>Conecta con las empresas asociadas en La Chambre</h1>
                            <p>Networking y el negocio entre nuestros socios.</p>
                        </div>
                            <form class="tourz-search-form">
                                <div class="input-field">
                                    <input type="text" id="select-city" class="autocomplete">
                                    <label for="select-city">Introduce la delegación</label>
                                </div>
                                <div class="input-field">
                                    <input type="text" id="select-search" class="autocomplete">
                                    <label for="select-search" class="search-hotel-type">Busca por empresa, sector o actividad</label>
                                </div>
                                <div class="input-field">
                                    <input type="submit" value="buscar" class="waves-effect waves-light tourz-sear-btn"> </div>
                            </form>
                    </div>
                </div>
            </div>
        </section>
    -->
    <section class="slider-app com-padd">
                <div class="row">
                    <div class="dir-hr1">
                        <div class="dir-ho-t-tit dir-ho-t-tit-2">
                            <h1 style="color: #2a2b33">Anuario digital de La Chambre</h1>
                            <p style="color: #2a2b33">Conecta y haz networking con las empresas asociadas.</p>
                        </div>
                    <div class="slideshow-container">

                          <!-- Full-width images with number and caption text -->
                          <div class="mySlides " style="display: block;">
                            <div class="numbertext">1 / 2</div>
                            <img src="images/air_france_top_banner.jpg" style="width:100%">
                          </div>

                          <div class="mySlides " style="display: none;">
                            <div class="numbertext">2 / 2</div>
                            <img src="images/slide.jpg" style="width:100%">
                          </div>


                          <!-- Next and previous buttons -->
                        <!--
                          <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                          <a class="next" onclick="plusSlides(1)">&#10095;</a>
                        -->
                        </div>
                        <br>

                        <!-- The dots/circles -->
                        <div style="text-align:center">
                          <span class="dot" onclick="currentSlide(1)"></span>
                          <span class="dot" onclick="currentSlide(2)"></span>
                          <span class="dot" onclick="currentSlide(3)"></span>
                        </div>

                    </div>
    </div></section>
    <!-- Slideshow container -->






        <!--FIND YOUR SERVICE-->
        <section class="cat-v2-hom com-padd mar-bot-red-m30">
            <div class="container">
                <div class="row">
                    <div class="com-title">
                        <h2>Encuentra servicios por categoría</h2>
                        <p>Explora negocios entre nuestros partners y filtra tu búsqueda por categoría.</p>
                    </div>
                    <div class="cat-v2-hom-list">
                        <ul>
                            <li>
                                <a href="#"><img src="images/icon/hcat1.png" alt=""> Abogados</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat2.png" alt=""> Administración</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat3.png" alt=""> Aeronáutica</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat4.png" alt=""> Agricultura</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat5.png" alt=""> AgroAlimentario</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat6.png" alt=""> Agua</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat7.png" alt=""> Arquitectura</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat8.png" alt=""> Arte</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat9.png" alt=""> Automóvil</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat10.png" alt=""> Banco</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat11.png" alt=""> Bienes de consumo</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat12.png" alt=""> Bienes inmuebles</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat13.png" alt=""> Biotecnología</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat14.png" alt=""> Comercio por menor</a>
                            </li>
                            <li>
                                <a href="#"><img src="images/icon/hcat15.png" alt=""> Compras</a>
                            </li>



                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--EXPLORE CITY LISTING-->
        <section class="com-padd com-padd-redu-top">
            <div class="container">
                <div class="row">
                    <div class="com-title">
                        <h2>Explora por delegación</h2>
                        <p>Encuentra los asociados de tu ciudad para conectar con ellos.</p>
                    </div>
                    <div class="col-md-6">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/madrid.jpg" alt=""> </div>
                                <div class="list-mig-lc-con">
                                    <h5>Madrid</h5>
                                    <p>205 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/bilbao.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Bilbao</h5>
                                    <p>125 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/valencia.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Valencia</h5>
                                    <p>66 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/sevilla.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Sevilla</h5>
                                    <p>205 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/malaga.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Málaga</h5>
                                    <p>44 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/baleares.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Baleares</h5>
                                    <p>44 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/zaragoza.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Zaragoza</h5>
                                    <p>44 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/valladolid.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Valladolid</h5>
                                    <p>44 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="list-lead.html">
                            <div class="list-mig-like-com">
                                <div class="list-mig-lc-img"> <img src="images/vigo.jpg" alt=""> </div>
                                <div class="list-mig-lc-con list-mig-lc-con2">
                                    <h5>Vigo</h5>
                                    <p>44 asociados</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--ADD BUSINESS-->
        <section class="com-padd home-dis">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Promocione aquí su negocio <a href="price.html">ir a tarifas</a></h2> </div>
                </div>
            </div>
        </section>
        <!--CREATE FREE ACCOUNT-->
    <!--
        <section class="com-padd sec-bg-white">
            <div class="container">
                <div class="row">
                    <div class="com-title">
                        <h2>Create a free <span>Account</span></h2>
                        <p>Explore some of the best tips from around the world from our partners and friends.</p>
                    </div>
                    <div class="col-md-6">
                        <div class="hom-cre-acc-left">
                            <h3>A few reasons you’ll love Online <span>Business Directory</span></h3>
                            <p>5 Benefits of Listing Your Business to a Local Online Directory</p>
                            <ul>
                                <li> <img src="images/icon/7.png" alt="">
                                    <div>
                                        <h5>Enhancing Your Business</h5>
                                        <p>Imagine you have made your presence online through a local online directory, but your competitors have..</p>
                                    </div>
                                </li>
                                <li> <img src="images/icon/5.png" alt="">
                                    <div>
                                        <h5>Advertising Your Business</h5>
                                        <p>Advertising your business to area specific has many advantages. For local businessmen, it is an opportunity..</p>
                                    </div>
                                </li>
                                <li> <img src="images/icon/6.png" alt="">
                                    <div>
                                        <h5>Develop Brand Image</h5>
                                        <p>Your local business too needs brand management and image making. As you know the local market..</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="hom-cre-acc-left hom-cre-acc-right">
                            <form>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="acc-name" type="text" class="validate">
                                        <label for="acc-name">Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="acc-mob" type="number" class="validate">
                                        <label for="acc-mob">Mobile</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="acc-mail" type="email" class="validate">
                                        <label for="acc-mail">Email</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="acc-pass" type="password" class="validate">
                                        <label for="acc-pass">Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 hom-cr-acc-check">
                                        <input type="checkbox" id="test5" />
                                        <label for="test5">By signing up, you agree to the Terms and Conditions and Privacy Policy. You also agree to receive product-related emails.</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12"> <a class="waves-effect waves-light btn-large full-btn" href="#!">Submit Now</a> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    -->
        <!--MOBILE APP-->
        <section class="web-app com-padd">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 web-app-img"> <img src="images/banner.jpg" alt=""> </div>
                    <div class="col-md-6 web-app-con">
                        <h2>¿Quiere conectar con más de 400 empresas?</h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Disfrute de ventajas y servicios preferentes.</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> La tarjeta Privilèges</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Ideas/propuestas de actividades.</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Comparta sus ambiciones, inquietudes y oportunidades de intercambio</li>
                        </ul> <span>Suscríbase a nuestro boletín para estar informado de todo lo que ocurre en La Chambre.</span>
                        <form>
                            <ul>
                                <li>
                                     </li>
                                <li>
                                    <input placeholder="Introduzca su email" type="text"> </li>
                                <li>
                                    <input value="Suscribir al boletín" type="submit"> </li>
                            </ul>
                        </form>
                                <div class="row">
                                    <div class="col s12 hom-cr-acc-check">
                                        <input id="test5" type="checkbox">
                                        <label for="test5">Acepto las condiciones de la política de privacidad y también que se me envíen notificaciones comerciales..</label>
                                    </div>
                    </div>
                </div>
            </div>
        </div></section>


        <!--FOOTER SECTION-->
        <footer id="colophon" class="site-footer clearfix">
            <div id="quaternary" class="sidebar-container " role="complementary">
                <div class="sidebar-inner">
                    <div class="widget-area clearfix">
                        <div id="azh_widget-2" class="widget widget_azh_widget">
                            <div data-section="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-3 foot-logo"> <img src="images/uccifeFooter.png" alt="logo">
                                            <p class="hasimg">Texto legal de la Cámara de comercio Franco Española</p>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <h4>Soporte y ayuda</h4>
                                            <ul class="two-columns">
                                                <li> <a href="advertise.html">Anúnciate</a> </li>
                                                <li> <a href="about-us.html">Nosotros</a> </li>
                                                <li> <a href="how-it-work.html">How it works </a> </li>
                                                <li> <a href="add-listing.html">Add Business</a> </li>
                                                <li> <a href="#!">Register</a> </li>
                                                <li> <a href="#!">Login</a> </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <h4>Servicios populares</h4>
                                            <ul class="two-columns">
                                                <li> <a href="#!">Hotels</a> </li>
                                                <li> <a href="#!">Hospitals</a> </li>
                                                <li> <a href="#!">Transportation</a> </li>
                                                <li> <a href="#!">Real Estates </a> </li>
                                                <li> <a href="#!">Automobiles</a> </li>
                                                <li> <a href="#!">Resorts</a> </li>
                                                <li> <a href="#!">Education</a> </li>
                                                <li> <a href="#!">Sports Events</a> </li>
                                                <li> <a href="#!">Web Services </a> </li>
                                                <li> <a href="#!">Skin Care</a> </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <h4>Delegaciones</h4>
                                            <ul class="two-columns">
                                                <li> <a href="#!">Madrid</a> </li>
                                                <li> <a href="#!">Sevilla</a> </li>
                                                <li> <a href="#!">Bilbao</a> </li>
                                                <li> <a href="#!">Zaragoza</a> </li>
                                                <li> <a href="#!">Valencia </a> </li>
                                                <li> <a href="#!">Baleares</a> </li>
                                                <li> <a href="#!">Canarias</a> </li>
                                                <li> <a href="#!">Vigo</a> </li>
                                                <li> <a href="#!">Málaga</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-section="section" class="foot-sec2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-5 foot-social">
                                            <h4>Síganos en Redes Sociales</h4>
                                            <ul>
                                                <li><a href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                                <li><a href="#!"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
                                                <li><a href="#!"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                                <li><a href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                                                <li><a href="#!"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                                <li><a href="#!"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .widget-area -->
                </div>
                <!-- .sidebar-inner -->
            </div>
            <!-- #quaternary -->
        </footer>
        <!--COPY RIGHTS-->
        <section class="copy">
            <div class="container">
                <p>copyrights © 2018 La Chambre &nbsp;&nbsp;All rights reserved. </p>
            </div>
        </section>
        <!--QUOTS POPUP-->
        <section>
            <!-- GET QUOTES POPUP -->
            <div class="modal fade dir-pop-com" id="list-quo" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header dir-pop-head">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Get a Quotes</h4>
                            <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                        </div>
                        <div class="modal-body dir-pop-body">
                            <form method="post" class="form-horizontal">
                                <!--LISTING INFORMATION-->
                                <div class="form-group has-feedback ak-field">
                                    <label class="col-md-4 control-label">Full Name *</label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="fname" placeholder="" required="" type="text"> </div>
                                </div>
                                <!--LISTING INFORMATION-->
                                <div class="form-group has-feedback ak-field">
                                    <label class="col-md-4 control-label">Mobile</label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="mobile" placeholder="" type="text"> </div>
                                </div>
                                <!--LISTING INFORMATION-->
                                <div class="form-group has-feedback ak-field">
                                    <label class="col-md-4 control-label">Email</label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="email" placeholder="" type="text"> </div>
                                </div>
                                <!--LISTING INFORMATION-->
                                <div class="form-group has-feedback ak-field">
                                    <label class="col-md-4 control-label">Message</label>
                                    <div class="col-md-8 get-quo">
                                        <textarea class="form-control"></textarea>
                                    </div>
                                </div>
                                <!--LISTING INFORMATION-->
                                <div class="form-group has-feedback ak-field">
                                    <div class="col-md-6 col-md-offset-4">
                                        <input value="SUBMIT" class="pop-btn" type="submit"> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- GET QUOTES Popup END -->
        </section>
        <!--SCRIPT FILES-->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/materialize.min.js" type="text/javascript"></script>
        <script src="js/custom.js"></script>
        <script src="js/carrusel.js"></script>
        <div class="hiddendiv common"></div></body>
</html>
