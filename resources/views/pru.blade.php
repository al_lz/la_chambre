@extends('app')
@section('content')

    <section id="menuresponsive">
        <div class="v3-mob-top-menu">
            <div class="container">
                <div class="row">
                    <div class="v3-mob-menu">
                        <div class="v3-mob-m-1">
                            <a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
                        </div>
                        <div class="v3-mob-m-2">
                          <div class="v3-top-ri">
  														<ul>
  															<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
  																	@auth
  																			<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
  																	@endauth
  																	@guest
  																			<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
  																	@endguest


  														</ul>
  												</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mob-right-nav" data-wow-duration="0.5s">
            <div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
            <!-- <h5>Business</h5> -->
            <ul class="mob-menu-icon">
              <li><a href="/">Home</a> </li>
              <li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
              @auth
                  <li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
              @endauth
              @guest
                  <li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
              @endguest
              <li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
              </li>
                <h5>La Chambre</h5>
                <ul>
                    <li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
                    <li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
                    <li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
                    <li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
                </ul>
            </ul>
        </div>
    </section>


    <section class="slider-app com-padd">
      <div class="row">
        <div class="dir-hr1">
          <div class="dir-ho-t-tit dir-ho-t-tit-2">
            <h1>Anuario digital de La Chambre</h1>
            <p>Su plataforma de negocios y networking franco-española.</p>
          </div>
          <carousel :autoplay="true">
            @foreach ($bannersHome as $banner)
            <div class="carousel-cell">
            <a href="{{ $banner->link }}" target="_blank" rel="aaaa">
                    <img src="{{ asset('storage/'.$banner->imagen) }}" alt="tech">

                </a>
            </div>
            @endforeach
          </carousel>
          <br>
          <!-- The dots/circles -->
          <div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
          </div>
        </div>
      </div>
    </section>
<!-- Slideshow container -->

@include('partials.homeServicios')

@include('partials.homeDelegaciones')





<section class="p-about com-padd">
		<div class="container">
			<div class="row">
        <div class="com-title"><h2>Testimonios de los socios</h2></div>
				<div class="pg-cus-rev">
					<div class="col-md-4">
						<div class="cus-rev">
							<em>El anuario digital de socios es unas herramienta que quería utilizar desde mi llegada a La Chambre el pasado verano. Me encantó la nueva versión por ser muy ágil de uso y estar muy bien diseñada, y por tener capacidad de realizar búsquedas de forma muy práctica, especialmente con multi-criterios. Además, tiene una gran riqueza en cuanto a empresas y sectores de actividades.</em>
							<div class="cus-re-com"> <img src="images/services/s1.jpeg" alt="">
								<h4>Pascal Portes</h4> <span>SAB</span> </div>
						</div>
					</div>
          <div class="col-md-4">
						<div class="cus-rev">
							<em>Formar parte del anuario digital de La Chambre nos ha proporcionado visibilidad como empresa de Marketing digital. Dentro de su listado de asociados te permite conectar con otras compañías y generar nuevas oportunidades de negocio.</em>
							<div class="cus-re-com"> <img src="images/services/s1.jpeg" alt="">
								<h4>Bruce Parnaudeau</h4> <span>Tywenn</span> </div>
						</div>
					</div>




				</div>
			</div>
		</div>
	</section>


    <!--EXPLORE CITY LISTING-->

    <!--ADD BUSINESS-->
    <section class="com-padd home-dis">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @auth
                        <h2>Promocione aquí su negocio  <a href="/tarifas" class="waves-effect waves-light btn">Solicite información</a></h2> </div>
                    @endauth
                    @guest
                        <h2>Promocione aquí su negocio  <a href="/contacto" class="waves-effect waves-light btn">Solicite información</a></h2> </div>
                    @endguest
            </div>
        </div>
    </section>

    <!--MOBILE APP-->


    @include('partials.homeBoletin')

    <!--QUOTS POPUP-->
    <section>
        <!-- GET QUOTES POPUP -->
        <div class="modal fade dir-pop-com" id="list-quo" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dir-pop-head">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Get a Quotes</h4>
                        <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                    </div>
                    <div class="modal-body dir-pop-body">
                        <form method="post" class="form-horizontal">
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Full Name *</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="fname" placeholder="" required="" type="text"> </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Mobile</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="mobile" placeholder="" type="text"> </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                    <input class="form-control" name="email" placeholder="" type="text"> </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Message</label>
                                <div class="col-md-8 get-quo">
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>
                            <!--LISTING INFORMATION-->
                            <div class="form-group has-feedback ak-field">
                                <div class="col-md-6 col-md-offset-4">
                                    <input value="SUBMIT" class="pop-btn" type="submit"> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- GET QUOTES Popup END -->
    </section>




@endsection
