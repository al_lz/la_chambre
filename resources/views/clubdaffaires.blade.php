@extends('app')

@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
												<div class="v3-top-ri">
														<ul>
															<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																	@auth
																			<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
																	@endauth
																	@guest
																			<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
																	@endguest


														</ul>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					<li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>


<section class="inn-page-bg">
		<div class="container">
			<div class="row">
				<div class="inn-pag-ban">
					<h2>Club d'Affaires</h2>
					<!-- <h5>Grow your business by getting relevant and verified leads</h5> </div> -->
			</div>
		</div>
	</section>
	<section class="p-about com-padd hom-cre-acc-left">
		<div class="container">
			<div class="row com-padd">
				<div class="col-md-6">
					<div class="page-about pad-bot-red-40">
            <h3>CLUB D’AFFAIRES FRANCE-ESPAGNE</h3>
            <span>Creado en 1977 con el nombre de Comité de Patrocinio de la Cámara Franco-Española de Comercio e Industria-La Chambre-, y ahora rebautizado como Club d’Affaires France-Espagne, está compuesto por los principales directivos de empresas con accionariado europeo implantadas en España que desean potenciar las actividades asociativas de La Chambre. Presidido por Dña. Sara Bieger, Socia Directora de AltoPartners, el Comité reúne a 63 empresas en la actualidad. </p></span>
            </div>
          </div>
				<div class="col-md-6">
					<div class="page-about"> <img src="images/club.jpg" alt=""> </div>
				</div>
			</div>
      <div class="row com-padd">
        <div class="col-md-12">
          <div class="row">
            <h4>A continuación, exponemos 5 ventajas de pertenecer al mismo: </h4>

            <div class="col-md-6">
              <ul>
                <li class="razones"><img src="images/potencia.png" alt="">
                  <div>
                    <h5>Potencie su imagen de marca al máximo nivel</h5>
                    <p>Esta categoría asociativa le permite vincular su imagen a la de una sólida asociación empresarial que aglutina a la mayor comunidad de negocios Franco-Española del país.</p>
                  </div>
                </li>
                <li class="razones"><img src="images/leads.png" alt="">
              <div>
                <h5>Acceda a una importante red de contactos</h5>
                <p>El Club d’Affaires France-Espagne aglutina a destacados directivos de las empresas que lo componen y con los que podrá establecer contactos en el transcurso de las diferentes actividades propuestas.</p>
              </div>
              </li>
                <li class="razones"><img src="images/grow.png" alt="">
              <div>
                <h5>Participe en un programa de actividades exclusivas</h5>
                <p>Además de la visibilidad que se le quiere dar de forma permanente desde La Chambre a los miembros del Club d’Affaires, consiste en la organización de 8-10 actividades exclusivas al año. Se trata generalmente de almuerzos que se llevan a cabo en el marco del Foro Roncesvalles, en los que se cuenta en cada ocasión con una personalidad invitada del mundo de la política, de la empresa, de los medios... con la que se debate de forma abierta pero confidencial. Se programan igualmente para las empresas de este Comité actividades de tipo más cultural -visitas guiadas privadas de exposiciones...-.</p>
              </div>
            </li>
              </ul>
            </div>


            <div class="col-md-6">
              <ul>
            <li class="razones"><img src="images/search.png" alt="">
              <div>
                <h5>Obtenga visibilidad y protagonismo permanente en la actividad cotidiana de la Cámara.</h5>
                <p>Durante el año 2017 se realizaron más de 100 actividades, casi la mitad de ellas en las diferentes Delegaciones CFECI. Además, el Club d’Affaires cuenta con una presencia continua en los elementos de comunicación de La Chambre tales como página web, roll-ups, trípticos descriptivos de La Chambre…</p>
              </div>
              </li>

            <li class="razones"><img src="images/ok.png" alt="">
              <div>
                <h5>Aporte conocimiento, saber hacer y apoyo financiero a La Chambre</h5>
                <p>Los miembros del Club d’Affaires poseen una dilatada experiencia y saber hacer en los complejos y dinámicos contextos empresariales francés y español. Cualidades que pueden poner al servicio del resto de empresas y emprendedores vinculados a La Chambre interviniendo, por ejemplo, como ponentes en charlas y seminarios.Este club permite también, gracias a la aportación económica de sus socios, el desarrollo de actividades asociativas de las cuales pueden disfrutar el resto de miembros de La Chambre, en su mayoría pymes.</p>
              </div>
            </li>
            </ul>
          </div>
        </div>
      </div>
		</div>
	</section>
	<section class="p-about-count">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 page-about-count">
					<div>
						<!-- <h4>Potencie su imagen de marca al máximo nivel</h4> -->
					</div>
				</div>
			</div>
		</div>
	</section>
@include('partials.homeBoletin')


@endsection
