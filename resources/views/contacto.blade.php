@extends('app')

@section('content')
<section id="menuresponsive">
		<div class="v3-mob-top-menu">
				<div class="container">
						<div class="row">
								<div class="v3-mob-menu">
										<div class="v3-mob-m-1">
												<a href="index.html"><img src="/images/lachambre.png" alt=""> </a>
										</div>
										<div class="v3-mob-m-2">
											<div class="v3-top-ri">
													<ul>
														<li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
																@auth
																		<li><a href="/logout" class="v3-menu-sign"><i class="fa fa-sign-out"></i> Salir</a> </li>
																@endauth
																@guest
																		<li><a href="/login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Acceso socios</a> </li>
																@endguest


													</ul>
											</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="mob-right-nav" data-wow-duration="0.5s">
				<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
				<!-- <h5>Business</h5> -->
				<ul class="mob-menu-icon">
					<li><a href="/">Home</a> </li>
					<li><a href="/empresas" data-toggle="modal">Empresas</a> </li>
					@auth
							<li><a class="" href="/tarifas" data-activates="drop-menu-page">Tarifas</a> </li>
					@endauth
					@guest
							<li><a class="" href="/contacto" data-activates="drop-menu-page">Tarifas</a> </li>
					@endguest
					<li><a class="" href="/contacto" data-activates="drop-menu-admin">Contacto</a>
					</li>
						<h5>La Chambre</h5>
						<ul>
								<li><a href="/quienes-somos"><i class="fa fa-angle-right" aria-hidden="true"></i> Quiénes somos</a> </li>
								<li><a href="/organizacion"><i class="fa fa-angle-right" aria-hidden="true"></i> Organización</a> </li>
								<li><a href="/clubdaffaires"><i class="fa fa-angle-right" aria-hidden="true"></i> Club d'Affaires</a> </li>
								<li><a href="/mensajepresidente"><i class="fa fa-angle-right" aria-hidden="true"></i> Mensaje del Presidente</a> </li>
						</ul>
				</ul>
		</div>
</section>
<section>
		<div class="con-page">
			<div class="con-page-ri">

				@include('partials.contactarLachambre')

        {{-- <div class="con-com">
					<h4 class="con-tit-top-o">Sede Central</h4>
					<p>Calle Capitán Haya 38 - 4ª, 28020 Madrid</p> <span class="icos"><i class="material-icons">phone</i> Tel: + 34 913 072 100</span> <span class="icos"><i class="material-icons">email</i> Email: lachambre@lachambre.es</span>

					<div class="con-page">
						<div class="cpn-pag-form">
							<form class="doblesrazones">
								<div>
									<div class="input-field col s12">
										<input id="gfc_name" type="text" class="validate" required="">
										<label for="gfc_name">Nombre y apellidos</label>
									</div>
								</div>
								<div>
									<div class="input-field col s12">
										<input id="gfc_mob" type="number" class="validate">
										<label for="gfc_mob">Tfno</label>
									</div>
								</div>
								<div>
									<div class="input-field col s12">
										<input id="gfc_mail" type="email" class="validate">
										<label for="gfc_mail">Email</label>
									</div>
								</div>
								<div>
									<div class="input-field col s12">
										<textarea id="gfc_msg" class="validate"></textarea>
										<label for="gfc_msg">Introduzca se mensaje</label>
									</div>
								</div>
								<div>
									<div class="input-field col s12">
										<i class="waves-effect waves-light btn-large full-btn list-red-btn waves-input-wrapper" style=""><input type="submit" value="Enviar" class="waves-button-input"></i> </div>
								</div>
							</form>
						</div>
					</div>

				</div> --}}



							<ul class="collapsible" data-collapsible="accordion">
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Valencia</div>
										<div class="collapsible-body" style="display: none;">
											<p>C/ Cronista Carreres, 11, 1º A, 46003</br> Valencia</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: + 96 394 31 06</span>
											<span class="icos"><i class="material-icons">email</i> Email: sgil@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Sandrine GIL</span>
										</div>
								</li>
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Málaga</div>
										<div class="collapsible-body" style="display: none;">
											<p>C/ Casas de Campos nº 4, 29001</br> Málaga</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: + 34 951 55 3244</span>
											<span class="icos"><i class="material-icons">email</i> Email: malaga@lachambre.ess</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Alejandro HERNÁNDEZ DEL CASTILLO</span>
										</div>
								</li>
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Valladolid</div>
										<div class="collapsible-body ico" style="display: none;">
											<p>Plaza Madrid, Nº6, Entreplanta Derecha, 47004</br> Valladolid</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: +34 666 73 37 15</span>
											<span class="icos"><i class="material-icons">email</i> Email: valladolid@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Carlos GONZÁLEZ-CASCOS</span>
										</div>
								</li>
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Bilbao</div>
										<div class="collapsible-body" style="display: none;">
											<p>C/Iparraguirre, 26 - 5 Planta, 48011</br> Bilbao</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: +34 94 652 38 896</span>
											<span class="icos"><i class="material-icons">email</i> Email: bilbao@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> </span>
										</div>
								</li>
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Zaragoza</div>
										<div class="collapsible-body" style="display: none;">
											<p></p>
											<span class="icos"><i class="material-icons">phone</i></span>
											<span class="icos"><i class="material-icons">email</i> Email: zaragoza@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> </span>
										</div>
								</li>
								<!-- <li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Vigo</div>
										<div class="collapsible-body" style="display: none;">
											<p>C/ Joaquín Loriga, 22, Entreplanta, 36203</br> Vigo</p>
											<span class="icos"><i class="material-icons">phone</i></span>
											<span class="icos"><i class="material-icons">email</i> Email: vigo@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Carlos QUINTANILLA LÓPEZ</span>
										</div>
								</li> -->
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Canarias</div>
										<div class="collapsible-body" style="display: none;">
											<p>C/ Robayna, 25 38004</br> Santa Cruz de Tenerife</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: +34 922 533 512</span>
											<span class="icos"><i class="material-icons">email</i> Email: canarias@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Francisco de ESTEBAN GARCÍA</span>
										</div>
								</li>
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Baleares</div>
										<div class="collapsible-body" style="display: none;">
											<p>Avda. Jaime III, nº3 - 2º- 1ª, 07012</br> Palma de Mallorca</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: +34 971 720 782</span>
											<span class="icos"><i class="material-icons">email</i> Email: baleares@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Noël E. LERYCKE</span>
										</div>
								</li>
								<li class="">
										<div class="collapsible-header"><i class="material-icons">place</i>Delegación Sevilla</div>
										<div class="collapsible-body" style="display: none;">
											<p>Plza. de Santa Cruz, 14, 1004</br> Sevilla</p>
											<span class="icos"><i class="material-icons">phone</i> Tel: +34 607 770 770</span>
											<span class="icos"><i class="material-icons">email</i> Email: sevilla@lachambre.es</span>
											<span class="icos"><i class="material-icons">personpin</i> Contacto: Christophe SOUGEY</span>
										</div>
								</li>
					</ul>





		</div>
	</section>


@endsection
