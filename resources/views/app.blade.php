<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src='https://www.googletagmanager.com/gtag/js?id=UA-117832205-2'></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag(‘js’, new Date());

   gtag(‘config’, ‘UA-117832205-2’);
  </script>


<title>Anuario digital La Chambre</title>
<script>
        var klubparke = <?php echo json_encode([ 'mapsKey' => config('voyager.googlemaps.key'), 'user' => auth()->user(), 'csrfToken' =>  csrf_token(), 'stripeKey' =>  config('services.stripe.key') ]) ?>;

    </script>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	@include('partials.metas')

	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
    <!-- el tema -->
	   <link rel="stylesheet" href="{{ asset('css/theme.css') }}">
	    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	     <link rel="stylesheet" href="{{ asset('css/gaizka.css') }}">
    <!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<!-- ALL CSS FILES -->
	<link href="{{ asset('css/materialize.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{ asset ('css/responsive.css') }}" rel="stylesheet"> --}}
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
    </div>
	<div id="lachambre_app">
    @include('partials.topmenu')
		@yield('content')
		{{-- @include('partials.mobileapp') --}}

		@include('partials.footer')

		<flash message="{{ session('flash') }}"></flash>
		 @include('partials.modalContacto')
	</div>

	<!--SCRIPT FILES-->
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/materialize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/custom.js') }}"></script>

	<script src="{{ asset('js/app.js') }}"></script>


</body>

</html>
