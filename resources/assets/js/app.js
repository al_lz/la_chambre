
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Event = new Vue();


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('carousel', require('./components/Carousel.vue'));
Vue.component('flash', require('./components/Flash.vue'));
Vue.component('filter-algolia', require('./components/FilterAlgolia.vue'));
Vue.component('banner-top', require('./components/BannerTop.vue'));
Vue.component('destacados', require('./components/Destacados.vue'));
Vue.component('contactar', require('./components/Contactar.vue'));
Vue.component('contactarlachambre', require('./components/ContactarLachambre.vue'));
Vue.component('chat-app', require('./components/chat/ChatApp.vue'));
Vue.component('conversation', require('./components/chat/Conversation.vue'));
Vue.component('contactlist', require('./components/chat/ContactList.vue'));
Vue.component('totalmessages', require('./components/chat/MenuMessages.vue'));

import InstantSearch from 'vue-instantsearch';
import VModal from 'vue-js-modal';

Vue.use(require('vue-moment'));
Vue.use(InstantSearch)
Vue.use(VModal)

const app = new Vue({
    el: '#lachambre_app'
});
