<?php

namespace App\Console\Commands;

use App\Mail\NewChatMessageMail;
use Illuminate\Console\Command;
use App\User;
use App\Message;
use Illuminate\Support\Facades\Mail;


class CheckUnreadMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:unread';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for unread messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
         // get all users except the authenticated one
         $contacts = User::all();

        //  var_dump($contacts->where('id',100)->first());die;
         foreach($contacts as $contact ){

             // get a collection of items where sender_id is the user who sent us a message
             // and messages_count is the number of unread messages we have from him
             $unreadIds = Message::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
              ->where('to', $contact->id )
            //  ->where('to', 2)
             ->where('read', false)
             ->groupBy('from')
             ->get();

             // FILTER NON EXISTING IDS
             $filtered = $unreadIds->filter(function($unread,$key) use ($contacts){
                     return $contacts->where('id',$unread->sender_id)->first();
             });

             if( $filtered->count() > 0){
                $this->info( $contact->email." tienes ".$filtered->count()." mensajes sin leer" );
                $contact->unread = $filtered->count();
                Mail::to($contact->email)->send(new NewChatMessageMail( $contact ));
             }
         }
         
        
    }
}
