<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;


class Client extends Model
{
    use Searchable;


    public function sector(){
        return $this->belongsTo('App\Sector');
    }

    public function delegations(){
        return $this->belongsToMany('App\Delegation');
    }

    public function toSearchableArray()
    {
        $data = $this->toArray();
        $data['sector'] = ($this->sector)?$this->sector->nombre:false;
        // $data['delegation'] = ($this->delegations)?$this->delegations->pluck('nombre')->toArray():false;
        $data['delegation'] = ($this->delegations)?$this->delegations->pluck('nombre'):false;
        

        return $data;
    }

    public function searchableAs()
    {
        return 'clientes_lachambre';
    }

}
