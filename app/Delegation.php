<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Delegation extends Model
{
    public function bannertop(){
        return $this->hasOne('App\BannerTop');
    }
    
    public function bannerdetail(){
        return $this->hasOne('App\BannerDetail');
    }

    public function clients()
    {
        return $this->belongsToMany('App\Client');
    }
}
