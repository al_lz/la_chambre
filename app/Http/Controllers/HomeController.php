<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Client;
use App\Mail\ContactarMail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectors = \App\Sector::all();
        $deletations = \App\Delegation::all();
        $bannersHome = \App\BannerHome::all();
        return view('pru', compact('sectors','bannersHome','deletations'));
    }

    public function asociados($asociado_id = null){
        if ($asociado_id) {
            $empresa = Client::findOrFail($asociado_id);
            $bannerdetail = \App\BannerDetail::first();
            $bannerdetailsector = \App\BannerDetail::where('sector_id', $empresa->sector->id )->get()->except(1);
            if ($empresa->delegations->count() > 0) {
                // dd($empresa->delegations->first()->id);
                $bannerdetaildelegation = \App\BannerDetail::where('delegation_id', $empresa->delegations->first()->id )->get();
            }else{
                $bannerdetaildelegation = false;
            }
            $array_coord = false;
            if($empresa->coordenadas){
                $array_coord = explode(',',$empresa->coordenadas);
            }
            $bannerdetail = \App\BannerDetail::first();
            return view('empresas.detail', compact(
                'empresa',
                'bannerdetail',
                'bannerdetailsector',
                'bannerdetaildelegation',
                'array_coord'));
        }else{
            $empresas = Client::all();
            $bannertop = \App\BannerTop::first();
            $bannersSector = \App\BannerTop::where('sector_id','!=',null)->get()->except(1);
            $bannersDelegation = \App\BannerTop::where('delegation_id','!=',null)->get();
            $destacados_general = Client::with('sector')->where('destacado_general',true)->limit(3)->get();
            $destacados_sector = $this->getDestacadosSector();
            return view('empresas.list', compact(
            'empresas',
                'bannertop',
                    'bannersSector',
                    'bannersDelegation',
                    'destacados_sector',
                    'destacados_general')
            );

        }
    }
    public function asociadosPorSector( \App\Sector $sector){
            
            $empresas = Client::all();
            $bannertop = \App\BannerTop::first();
            $bannersSector = \App\BannerTop::where('sector_id','!=',null)->get()->except(1);
            $bannersDelegation = \App\BannerTop::where('delegation_id','!=',null)->get();
            $destacados_general = Client::with('sector')->where('destacado_general',true)->limit(3)->get();
            $destacados_sector = $this->getDestacadosSector();
            // dd($bannersDelegation);
            // dd($sector);
            return view('empresas.listporsector', compact('empresas','bannertop','bannersSector','bannersDelegation','sector','destacados_sector','destacados_general'));
    }
    public function asociadosPorDelegacion( \App\Delegation $delegation){
            
        $empresas = Client::all();
        $bannertop = \App\BannerTop::first();
        $bannersSector = \App\BannerTop::where('sector_id','!=',null)->get()->except(1);
        $bannersDelegation = \App\BannerTop::where('delegation_id','=',$delegation->id)->get();
        $destacados_general = Client::with('sector')->where('destacado_general',true)->limit(3)->get();
        $destacados_sector = $this->getDestacadosSector();
        // dd($bannersDelegation);
        // dd($sector);
        return view('empresas.listpordelegacion', compact('empresas','bannertop','bannersSector','bannersDelegation','delegation','destacados_sector','destacados_general'));
}

    

    public function getDestacadosSector(){
        $sectores_clientes = \App\Sector::with(['clientes' => function ($query) {
            //dd($query);
            $query->where('destacado_sector', true);
        }])->get();
    //    dd($sectores_clientes);
        $destacados_sector = [];
        foreach ($sectores_clientes as $key => $sector) {
            // print($sector->nombre)." ".$sector->clientes->count()."</br>";
            if($sector->clientes->count() > 0){
                $destacados_sector[] = $sector;
            }
            
        }
        // dd($destacados_sector);
        return collect($destacados_sector);
    }

    public function contactar(Request $request)
    {
        if ($request->input('client_id')) {
            # code...
            $client = Client::find($request->input('client_id'));
        }
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        $data = $request->all();
        // dd($client);
        if ($request->input('client_id')) {
            Mail::to($client->email)->send(new ContactarMail( $data ));
        }else{
            Mail::to('lachambre@lachambre.es')->send(new ContactarMail( $data ));
        }
        // echo $request->input('name');
    }

}
