<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Sector extends Model
{
    public function bannertop(){
        return $this->hasOne('App\BannerTop');
    }
    
    public function bannerdetail(){
        return $this->hasOne('App\BannerDetail');
    }

    public function clientes(){
        return $this->hasMany('App\Client');
    }
    
}
