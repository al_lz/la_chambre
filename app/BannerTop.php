<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BannerTop extends Model
{
    protected $table = "banner_top";
    protected $appends = ['nombre_sector','nombre_delegacion'];

    
    public function getNombreSectorAttribute(){
        if ($this->sector) {
            # code...
            return $this->sector->nombre;
        }
        return false;
    }
    public function getNombreDelegacionAttribute(){
        if ($this->delegation) {
            # code...
            return $this->delegation->nombre;
        }
        return false;
    }

    public function sector(){
        return $this->belongsTo('App\Sector');
    } 
    public function delegation(){
        return $this->belongsTo('App\Delegation');
    } 


}
